package jp.co.nri.agile.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.nri.agile.service.FeedbackService;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;

@Component
@RequestMapping( value = "eve/{eveID:[0-9]+/pre}/")
public class FeedbackController {
		
	@Autowired
	FeedbackService feedbackService;

	@Value("${name:World}")
	private String name;

	@RequestMapping( value = "{preID:^[0-9]+$}", method = { RequestMethod.GET })
	public String index(@PathVariable("eveID") Long eveID, @PathVariable("preID") Long preID) {
		
		//評価項目一覧を取得してResponseとして登録する
		
		
		return "FeedbackController/index";
	}
	
}
