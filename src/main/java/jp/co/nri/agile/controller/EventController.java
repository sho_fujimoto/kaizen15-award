package jp.co.nri.agile.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import jp.co.nri.agile.service.EventService;
import jp.co.nri.agile.service.PresentationContentService;
import jp.co.nri.agile.entity.Event;
import jp.co.nri.agile.entity.PresentationContent;

@Component
@RequestMapping( value = "eve/{eveID:[0-9]+}/")
public class EventController {
		
	@Autowired
	EventService eventService;

	@Autowired
	PresentationContentService presentationContentService;

	@RequestMapping( method = { RequestMethod.GET })
	public String index(@PathVariable("eveID") Long eveID, Model model) {
		
		//イベントを取得してResponseとして登録する
		Event eve = eventService.find(eveID);
		model.addAttribute("eve", eve);

		//TODO 発表内容一覧を取得して、Responseとして登録する
		List<PresentationContent> preList = presentationContentService.findByEventID(eveID);
		model.addAttribute("preList", preList);
		
		return "EventController/index";

	}
	
}
