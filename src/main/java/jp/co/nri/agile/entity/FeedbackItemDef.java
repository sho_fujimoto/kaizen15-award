package jp.co.nri.agile.entity;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;



@Entity
@Table(name="FeedbackItemDefTable")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackItemDef {

  private Long EventID;  
  @JoinColumn(table = "EventTable", name = "ID")

  @Size(min = 1, max = 255)
  private String ItemID;
  
  @NotNull
  @Size(min = 1, max = 255)
  private String Title;
  
  @NotNull
  private Long DisplayOrder;
  
  @NotNull
  private Long itemType;   
  
  private Long MinValue;
  
  private Long MaxValue;
  
  @NotNull
  private boolean Required;
  

  

  
 
}