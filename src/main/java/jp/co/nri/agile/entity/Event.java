package jp.co.nri.agile.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name="EventTable")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {

    @Id
    @GeneratedValue
    private Long ID;

    @NotNull
    @Size(min = 1, max = 255)
    private String Title;
  
    private Date EventDate;

    @Size(min = 1, max = 255)  
    private String Place;
	
    
	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public String getTitle() {
		return Title;
	}
	
	public void setTitle(String title) {
		Title = title;
	}
	
	public Date getEventDate() {
		return EventDate;
	}
	
	public void setEventDate(Date eventDate) {
		EventDate = eventDate;
	}
	
	public String getPlace() {
		return Place;
	}
	
	public void setPlace(String place) {
		Place = place;
	}
  
 
}
