package jp.co.nri.agile.entity;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;



@Entity
@Table(name="PresentationContentTable")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PresentationContent {

    @Id
    @GeneratedValue
    private Long ID;
  
    @NotNull
    private Long EventID;  
    @JoinColumn(table = "EventTable", name = "ID")

    @Size(min = 1, max = 255)
    private String Title;
  
    @Size(min = 1, max = 255)
    private String Content;
  
    @Size(min = 1, max = 255)
    private String Presenter;
  
    @NotNull
    private Long Status;   
  
    @Size(min = 1, max = 255)
    private String Comment;
  
    @NotNull
    private Long DisplayOrder;

	public Long getID() {
		return ID;
	}

	public void setID(Long iD) {
		ID = iD;
	}

	public Long getEventID() {
		return EventID;
	}

	public void setEventID(Long eventID) {
		EventID = eventID;
	}

	public String getTitle() {
		return Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}

	public String getPresenter() {
		return Presenter;
	}

	public void setPresenter(String presenter) {
		Presenter = presenter;
	}

	public Long getStatus() {
		return Status;
	}

	public void setStatus(Long status) {
		Status = status;
	}

	public String getComment() {
		return Comment;
	}

	public void setComment(String comment) {
		Comment = comment;
	}

	public Long getDisplayOrder() {
		return DisplayOrder;
	}

	public void setDisplayOrder(Long displayOrder) {
		DisplayOrder = displayOrder;
	}  
  
  

  
 
}