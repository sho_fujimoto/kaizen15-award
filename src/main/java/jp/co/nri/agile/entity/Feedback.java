package jp.co.nri.agile.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="FeedbackTable")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Feedback {

  @Id
  private Long UserID;
  
  private Long EventID;  
  @JoinColumn(table = "EventTable", name = "ID")
  
  private Long PresentationID;  
  @JoinColumn(table = "PresentationContentTable", name = "ID")

  @Size(min = 1, max = 255)
  private Long FeedbackItemID;
  
  @Size(min = 1, max = 255)
  private String Value;
 
  @NotNull
  private Timestamp SendTime;
  
  

  
 
}