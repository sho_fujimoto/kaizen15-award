package jp.co.nri.agile.repository;

import jp.co.nri.agile.entity.PresentationContent;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface PresentationContentRepository extends JpaRepository<PresentationContent, Long> {

	//find method
	public List<PresentationContent> findByEventID(long eventID);
	
}