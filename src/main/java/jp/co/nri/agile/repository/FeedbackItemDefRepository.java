package jp.co.nri.agile.repository;

import jp.co.nri.agile.entity.FeedbackItemDef;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface FeedbackItemDefRepository extends JpaRepository<FeedbackItemDef, Long> {

}