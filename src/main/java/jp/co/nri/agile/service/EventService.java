package jp.co.nri.agile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.nri.agile.entity.Event;
import jp.co.nri.agile.repository.EventRepository;

import java.util.List;

@Service
@Transactional
public class EventService {

  @Autowired 
  EventRepository eventRepository;

  public List<Event> findAll() {
    return eventRepository.findAll(new Sort(Sort.Direction.ASC, "ID"));
  }

  public Event save(Event event) {
    return eventRepository.save(event);
  }

  public void delete(Long id) {
    eventRepository.delete(id);
  }

  public Event find(Long id) {
        return eventRepository.findOne(id);
    }
}