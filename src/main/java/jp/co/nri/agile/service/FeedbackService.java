package jp.co.nri.agile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.nri.agile.entity.Feedback;
import jp.co.nri.agile.repository.FeedbackRepository;

import java.util.List;

@Service
@Transactional
public class FeedbackService {

  @Autowired 
  FeedbackRepository feedbackRepository;

  public List<Feedback> findAll() {
    return feedbackRepository.findAll(new Sort(Sort.Direction.ASC, "ID"));
  }

  public Feedback save(Feedback feedback) {
    return feedbackRepository.save(feedback);
  }

  public void delete(Long id) {
    feedbackRepository.delete(id);
  }

  public Feedback find(Long id) {
        return feedbackRepository.findOne(id);
    }
}