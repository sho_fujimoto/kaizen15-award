package jp.co.nri.agile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.nri.agile.entity.PresentationContent;
import jp.co.nri.agile.repository.PresentationContentRepository;

import java.util.List;

@Service
@Transactional
public class PresentationContentService {

  @Autowired 
  PresentationContentRepository presentationContentRepository;

  public List<PresentationContent> findAll() {
    return presentationContentRepository.findAll(new Sort(Sort.Direction.ASC, "ID"));
  }

  public List<PresentationContent> findByEventID(Long eventID) {
	  //TODO ソートキーの指定方法
	  return presentationContentRepository.findByEventID(eventID);
  }
  
  public PresentationContent save(PresentationContent presentationContent) {
    return presentationContentRepository.save(presentationContent);
  }

  public void delete(Long id) {
    presentationContentRepository.delete(id);
  }

  public PresentationContent find(Long id) {
        return presentationContentRepository.findOne(id);
    }
}