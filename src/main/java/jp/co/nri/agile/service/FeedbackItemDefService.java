package jp.co.nri.agile.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.nri.agile.entity.FeedbackItemDef;
import jp.co.nri.agile.repository.FeedbackItemDefRepository;

import java.util.List;

@Service
@Transactional
public class FeedbackItemDefService {

  @Autowired 
  FeedbackItemDefRepository feedbackItemDefRepository;

  public List<FeedbackItemDef> findAll() {
    return feedbackItemDefRepository.findAll(new Sort(Sort.Direction.ASC, "ID"));
  }

  public FeedbackItemDef save(FeedbackItemDef feedbackItemDef) {
    return feedbackItemDefRepository.save(feedbackItemDef);
  }

  public void delete(Long id) {
    feedbackItemDefRepository.delete(id);
  }

  public FeedbackItemDef find(Long id) {
        return feedbackItemDefRepository.findOne(id);
    }
}