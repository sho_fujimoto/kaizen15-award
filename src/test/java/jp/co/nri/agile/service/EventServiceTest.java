package jp.co.nri.agile.service;

import java.util.Date;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import jp.co.nri.agile.entity.Event;
import jp.co.nri.agile.service.EventService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EventServiceTest {

	@Autowired
	private EventService es;

	@Test
	public void add() {
		Event entity1 = new Event();
		Event entity2;

		//レコードの登録
		entity1.setTitle("テストイベント");
		entity1.setEventDate(new Date());
		entity1.setPlace("東京都新宿区");
		es.save(entity1);

		//レコードの取得
		entity2 = es.find(entity1.getID());
		assertEquals(entity1.getID(),entity2.getID());
		assertEquals(entity1.getTitle(),entity2.getTitle());
		assertEquals(entity1.getEventDate(),entity2.getEventDate());
		assertEquals(entity1.getPlace(),entity2.getPlace());

	}

}
