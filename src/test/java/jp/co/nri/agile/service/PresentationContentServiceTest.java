package jp.co.nri.agile.service;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import jp.co.nri.agile.entity.Event;
import jp.co.nri.agile.entity.PresentationContent;
import jp.co.nri.agile.service.PresentationContentService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PresentationContentServiceTest {

	@Autowired
	private PresentationContentService svc;
	
	@Autowired
	private EventService ec;

	@Test
	public void add() {
		
		Event ev = new Event();
		ev.setTitle("テストイベント");
		ev.setEventDate(new Date());
		ev.setPlace("東京都新宿区");
		ec.save(ev);

		PresentationContent entity1 = new PresentationContent();
		PresentationContent entity2;

		//レコードの登録
		entity1.setTitle("テスト発表");
		entity1.setEventID(ev.getID());
		entity1.setContent("テスト発表の内容を文字で表現したもの");
		entity1.setDisplayOrder((long)1);
		entity1.setPresenter("テスト　ユーザ");
		entity1.setStatus((long)1);
		svc.save(entity1);

		//レコードの取得
		entity2 = svc.find(entity1.getID());
		assertEquals(entity1.getID(),entity2.getID());
		assertEquals(entity1.getTitle(),entity2.getTitle());
		assertEquals(entity1.getContent(),entity2.getContent());
		assertEquals(entity1.getPresenter(),entity2.getPresenter());
		assertEquals(entity1.getStatus(),entity2.getStatus());

	}

}
