module project {
    export class Anim {
        get x():number {
            return this.baseX + Math.sin(this.frame);
        }
        constructor() {
            this.size = Math.random() * 3 + 1;
            this.dy = Math.random() * 1 + 0.5;
            this.frame = 0;
            this.image_path = 'assets/web_heart_animation.png';
            this.img_x = 100;
            this.img_y = 100;
            this.max_frame = 29;
        }
        public baseX:number;
        public y:number;
        public size:number;
        public frame:number;
        public dy:number;
        public image_path:string;
        public img_x:number;
        public img_y:number;
        public max_frame:number;
    }
}