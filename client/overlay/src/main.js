"use strict";
var project;
(function (project) {
    var Main = (function () {
        function Main() {
            var _this = this;
            this.animList = [];
            this.createAnim = function () {
                for (var i = 0; i < 100; i++) {
                    var anim = new Anim();
                    _this.animList.push(anim);
                    anim.baseX = _this.myCanvas.width * Math.random();
                    anim.y = _this.myCanvas.height * Math.random() - _this.myCanvas.height;
                }
            };
            this.update = function () {
                var animListLength = _this.animList.length;
                for (var i = 0; i < animListLength; i++) {
                    var anim = _this.animList[i];
                    anim.y += anim.dy;
                    if (anim.y >= -anim.size) {
                        anim.frame += 0.1;
                    }
                    if (anim.y >= _this.myCanvas.height + anim.size) {
                        anim.y -= _this.myCanvas.height - anim.size;
                        anim.baseX = _this.myCanvas.width * Math.random();
                    }
                }
            };
            this.draw = function () {
                _this.context2d.clearRect(0, 0, _this.myCanvas.width, _this.myCanvas.height);
                _this.context2d.fillStyle = "rgb(255,255,255)";
                var animListLength = _this.animList.length;
                for (var i = 0; i < animListLength; i++) {
                    var anim = _this.animList[i];
                    _this.context2d.beginPath();
                    _this.context2d.arc(anim.x, anim.y, anim.size, 0, Math.PI * 2, false);
                    _this.context2d.fill();
                    _this.context2d.closePath();
                }
            };
            this.myCanvas = document.getElementById('myCanvas');
            this.context2d = this.myCanvas.getContext('2d');
            this.myCanvas.width = document.documentElement.clientWidth;
            this.myCanvas.height = document.documentElement.clientHeight;
            this.createAnim();
        }
        return Main;
    }());
    project.Main = Main;
})(project || (project = {}));
var project;
(function (project) {
    var Snow = (function () {
        function Snow() {
            this.size = Math.random() * 3 + 1;
            this.dy = Math.random() * 1 + 0.5;
            this.frame = 0;
        }
        Object.defineProperty(Snow.prototype, "x", {
            get: function () {
                return this.baseX + Math.sin(this.frame);
            },
            enumerable: true,
            configurable: true
        });
        return Snow;
    }());
    project.Snow = Snow;
})(project || (project = {}));
