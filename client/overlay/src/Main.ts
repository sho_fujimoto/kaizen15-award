"use strict";

/// <reference path="./Anim.ts" />
module project {

    export class Main {
        private context2d:CanvasRenderingContext2D;
        private myCanvas:HTMLCanvasElement;
        private animList:Anim[] = [];

        constructor() {

            this.myCanvas = <HTMLCanvasElement>document.getElementById('myCanvas');
            this.context2d = <CanvasRenderingContext2D>this.myCanvas.getContext('2d');

            this.myCanvas.width = document.documentElement.clientWidth;
            this.myCanvas.height = document.documentElement.clientHeight;

            this.createAnim();
        }

        private createAnim = () => {
            for (var i = 0; i < 100; i++) {
                var anim = new Anim();
                this.animList.push(anim);
                anim.baseX = this.myCanvas.width * Math.random();
                anim.y = this.myCanvas.height * Math.random() - this.myCanvas.height;
            }
        }

        public update = () => {
            let animListLength = this.animList.length;
            for (var i = 0; i < animListLength; i++) {
                var anim:Anim = this.animList[i];
                anim.y += anim.dy;

                if (anim.y >= -anim.size) {
                    anim.frame += 0.1;
                }
                if (anim.y >= this.myCanvas.height + anim.size) {
                    anim.y -= this.myCanvas.height - anim.size;
                    anim.baseX = this.myCanvas.width * Math.random();
                }
            }
        }
        public draw = () => {

            this.context2d.clearRect(0, 0, this.myCanvas.width, this.myCanvas.height);
            this.context2d.fillStyle = "rgb(255,255,255)";

            let animListLength = this.animList.length;
            for (var i = 0; i < animListLength; i++) {
                var anim:Anim = this.animList[i];
                this.context2d.beginPath();
                this.context2d.arc(anim.x, anim.y, anim.size, 0, Math.PI * 2, false);
                this.context2d.fill();
                this.context2d.closePath();
            }
        }

    }
}